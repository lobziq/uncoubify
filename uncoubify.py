import requests
import clipboard
import bs4
import subprocess

source = 'http://savieo.com'
r = requests.get(source + '/download', params={'url': clipboard.paste()})
video = source + list(filter(lambda a: a['href'].startswith('/output'), bs4.BeautifulSoup(r.text, 'html.parser').find_all('a')))[0]['href']
audio = list(filter(lambda a: 'coub' and '.mp3' in a['href'], bs4.BeautifulSoup(r.text, 'html.parser').find_all('a')))[0]['href']
command = 'ffmpeg -i {0} -i {1} -shortest {2}.mp4'.format(video, audio, clipboard.paste().split('/')[-1])
subprocess.call(command.split())